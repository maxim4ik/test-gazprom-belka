import Vue from "vue";
import VueRouter from "vue-router";
import Main from "../pages/Main";
import Task3 from "../pages/Task3";
import Task2 from "../pages/Task2";
import Task1 from "../pages/Task1";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Main",
    component: Main
  },
  {
    path: "/task1",
    name: "Task2",
    component: Task1
  },
  {
    path: "/task2",
    name: "Task2",
    component: Task2
  },
  {
    path: "/task3",
    name: "Task3",
    component: Task3
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
